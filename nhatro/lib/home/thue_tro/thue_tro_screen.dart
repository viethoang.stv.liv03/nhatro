import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:nhatro/home/thue_tro/item_thue_tro.dart';
import 'package:nhatro/home/thue_tro/tao_phong.dart';
import 'package:nhatro/model/phong.dart';
import 'package:nhatro/model/user.dart';

class ThueTroScreen extends StatefulWidget {
  User userCurrent;
  ThueTroScreen({Key key, this.userCurrent}) : super(key: key);
  @override
  _ThueTroScreenState createState() => _ThueTroScreenState();
}

class _ThueTroScreenState extends State<ThueTroScreen> {
  Color _colorApp = Color.fromARGB(255, 45, 53, 110);
  var options = [
    'Giá tiền',
    'Rating',
    'Địa chỉ',
    'Diện tích',
    'Xung quanh tôi'
  ];
  String defaultGender = 'Tuỳ chọn';
  var xungQuanh = ['500m', '1km', '3km', '5km'];
  String defaultXungQuanh = 'Số ';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: Stack(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: 55,
                color: Colors.grey[400],
                child: GestureDetector(
                  onTap: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        child: Text(
                          defaultGender,
                          style: TextStyle(
                            fontSize: 16,
                            color: Color.fromARGB(255, 45, 53, 110),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            // isExpanded: true,
                            icon: Icons.arrow_drop_down,
                            colorIcon: Color.fromARGB(255, 45, 53, 110),
                            isDense: true,
                            onChanged: (String newValueSelected) {
                              setState(() {
                                defaultGender = newValueSelected;
                              });
                            },
                            items: options.map((String dropDownListItem) {
                              return DropdownMenuItem<String>(
                                value: dropDownListItem,
                                child: Text(
                                  dropDownListItem,
                                  style: TextStyle(
                                      color: Color.fromARGB(255, 45, 53, 110),
                                      fontSize: 16),
                                ),
                              );
                            }).toList(),
                            // value: defaultGender,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          defaultGender == "Xung quanh tôi"
              ?
               
              Container(
                width: MediaQuery.of(context).size.width,
                height: 40,
                color: Colors.grey,
                padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(left: 10, right: 20, top: 60,),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      // isExpanded: true,
                      icon: Icons.arrow_drop_down,
                      colorIcon: Color.fromARGB(255, 45, 53, 110),
                      isDense: true,
                      onChanged: (String newValueSelected) {
                        setState(() {
                          defaultXungQuanh = newValueSelected;
                        });
                      },
                      items: xungQuanh.map((String dropDownListItem) {
                        return DropdownMenuItem<String>(
                          value: dropDownListItem,
                          child: Text(
                            dropDownListItem,
                            style: TextStyle(
                                color: Color.fromARGB(255, 45, 53, 110),
                                fontSize: 16),
                          ),
                        );
                      }).toList(),
                      value: defaultXungQuanh,
                    ),
                  ),
                )
              : Container(),
          Container(
            margin: EdgeInsets.fromLTRB(10, 100, 10, 0),
            child: RaisedButton(
              color: Colors.grey[400],
              child: Text('Đăng phòng trọ'),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => CreateRoomScreen(
                          userCurrent: widget.userCurrent,
                        )));
              },
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 150),
            child: StreamBuilder<QuerySnapshot>(
              stream:
                  Firestore.instance.collection('listPhongChoThue').snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                      child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(_colorApp),
                  ));
                } else {
                  return snapshot.data.documents.length == 0
                      ? Container()
                      : ListView.builder(
                          itemBuilder: (context, index) {
                            return ThueTroItem(
                              phong: Phong(
                                  diaChi: snapshot.data.documents[index]
                                      ['diaChi'],
                                  dienTich: snapshot.data.documents[index]
                                      ['dienTich'],
                                  gia: snapshot.data.documents[index]['gia'],
                                  loaiNhaTro: snapshot.data.documents[index]
                                      ['loaiNhaTro'],
                                  moTa: snapshot.data.documents[index]['moTa'],
                                  sdt: snapshot.data.documents[index]['sdt'],
                                  sucChua: snapshot.data.documents[index]
                                      ['sucChua'],
                                  uidOfHost: snapshot.data.documents[index]
                                      ['uidOfHost'],
                                  userNameOfHost: snapshot.data.documents[index]
                                      ['userNameOfHost'],
                                  avatarOfHost: snapshot.data.documents[index]
                                      ['avatarOfHost'],
                                  image: snapshot.data.documents[index]
                                      ['image'],
                                  rating: snapshot.data.documents[index]
                                      ['rating']),
                            );
                          },
                          itemCount: snapshot.data.documents.length,
                        );
                }
              },
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
